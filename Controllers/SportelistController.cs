﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POSTA1.Models;
using System.Data.Entity.Validation;
using System.Web.Security;

namespace POSTA1.Controllers
{
    [Authorize(Roles = "Sportelist")]
    public class SportelistController : Controller
    {
        public static PostaEntities7 db = new PostaEntities7();

        public int GetId()
        {
            var emri = User.Identity.Name;

            var query = from f in db.Userats
                        where f.UserName == emri
                        select f;

            int id2 = Convert.ToInt32(query.FirstOrDefault().UID);
            return id2;
        }

        public int GetSid()
        {
            var emri = User.Identity.Name;

            var query = from f in db.Userats
                        where f.UserName == emri
                        select f;

            int id2 = Convert.ToInt32(query.FirstOrDefault().UID);
            var user = from q in db.Stafis
                       where q.UserID == id2
                       select q;
            int id1 = Convert.ToInt32(user.FirstOrDefault().SID);
            return id1;



        }
        // GET: Admin
        [HttpGet]
        public ActionResult Index()
        {


            int sid = GetSid();

            Stafi st = db.Stafis.Where(x => x.SID == sid).FirstOrDefault();
            return View(st);




        }
        [HttpPost]
        public ActionResult Index(string pass, string passIRI)
        {
            int id = GetId();

            Stafi st = db.Stafis.FirstOrDefault(a => a.UserID == id);


            if (pass == passIRI)
            {
                Userat u = (from x in db.Userats
                            where x.UID == id
                            select x).First();
                u.Fjalekalimi = pass;

                db.SaveChanges();

            }
            else
            {
                TempData["Message"] = "Fjalekalimet duhet te jene te njejt";
            }

            return View(st);
        }
        /////===========================================================================================================================

        [HttpGet]
    public ActionResult RegjistroDV()
        {
            ViewBag.Qytetet = new SelectList(db.Qytetets.ToList(), "QYTETIID", "EmriQytetit");
        
            ViewBag.Userat = new SelectList(db.Userats.ToList(), "UID", "UserName");
            ViewBag.Sherbimet = new SelectList(db.LlojiSherbimits.ToList(), "SHERBIMIID", "EmriSherbimit");
            ViewBag.Qmimi = new SelectList(db.Qmimis.ToList(), "CMIMIID", "Cmimi");
            ViewBag.Peshat = new SelectList(db.Peshats.ToList(), "PESHAID", "Numri");
            ViewBag.Zyrat = new SelectList(db.ZyraPostares.ToList(), "ZPID", "EmriZyres");
           


            return View();
        }




        [HttpPost]
        public ActionResult RegjistroDV(DergesatVendore dv)
        {

            return RedirectToAction("RegjistroDV2",dv);
        }
        [HttpGet]
        public ActionResult RegjistroDV2(DergesatVendore dv)
        {
            int query = (from q in db.Qmimis
                         where q.SherbimiId == dv.LlojiSherbimitId && q.PeshaId == dv.PeshaId && q.Trafiku == "Vendor"
                         select q.CMIMIID).FirstOrDefault();
            dv.QmimiId = query;

            var query2 = (from q in db.Qmimis
                          where q.CMIMIID == query
                          select q.Cmimi).FirstOrDefault();

            ViewBag.qmimi = query2;

            return View();
        }

        [HttpPost]
        [ActionName("RegjistroDV2")]
        public ActionResult RegjistroDV2Post(DergesatVendore dv1)
        {
            int id = GetId();
            int query1 = (from s in db.Stafis
                          where s.UserID == id
                          select s.ZpID).FirstOrDefault();


            int query = (from q in db.Qmimis
                         where q.SherbimiId == dv1.LlojiSherbimitId && q.PeshaId == dv1.PeshaId && q.Trafiku == "Vendor"
                         select q.CMIMIID).FirstOrDefault();

            
            DergesatVendore dv = new DergesatVendore();
            try
            {
                dv.NrRekomandes = dv1.NrRekomandes;
                dv.LlojiSherbimitId = dv1.LlojiSherbimitId;
                dv.PeshaId = dv1.PeshaId;
                dv.QytetiId = dv1.QytetiId;
                dv.UserId = id;
                dv.Derguesi = dv1.Derguesi;
                dv.AdresaDerguesit = dv1.AdresaDerguesit;
                dv.Marresi = dv1.Marresi;
                dv.AdresaMarresit = dv1.AdresaMarresit;
                dv.DataRegj = DateTime.Today;
                dv.QmimiId = query;
                dv.ZpId = query1;
                db.DergesatVendores.Add(dv);
                db.SaveChanges();

                return RedirectToAction("ListaDV");
            }
            catch (DbEntityValidationException ex)
            {
                 var errorMessages = ex.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);
    
            // Join the list to a single string.
            var fullErrorMessage = string.Join("; ", errorMessages);
    
            // Combine the original exception message with the new one.
            var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);
    
            // Throw a new DbEntityValidationException with the improved exception message.
            throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
        
            }

            //return View();
        }
        ///================================================================================================================================
        [HttpGet]
        public ActionResult RegjistroDN()
        {
            ViewBag.Shtetet = new SelectList(db.Shtetets.ToList(), "SHTETIID", "EmriShtetit");

            ViewBag.Userat = new SelectList(db.Userats.ToList(), "UID", "UserName");
            ViewBag.Sherbimet = new SelectList(db.LlojiSherbimits.ToList(), "SHERBIMIID", "EmriSherbimit");
            ViewBag.Qmimi = new SelectList(db.Qmimis.ToList(), "CMIMIID", "Cmimi");
            ViewBag.Peshat = new SelectList(db.Peshats.ToList(), "PESHAID", "Numri");
            ViewBag.Zyrat = new SelectList(db.ZyraPostares.ToList(), "ZPID", "EmriZyres");



            return View();
        }




        [HttpPost]
        public ActionResult RegjistroDN(DergesatNderkombetare dn)
        {

            return RedirectToAction("RegjistroDN2", dn);
        }
        [HttpGet]
        public ActionResult RegjistroDN2(DergesatNderkombetare dn)
        {

            int query = (from q in db.Qmimis
                         where q.SherbimiId == dn.LlojiSherbimitId && q.PeshaId == dn.PeshaId && q.Trafiku == "Nderkombetare"
                         select q.CMIMIID).FirstOrDefault();
            dn.QmimiId = query;

            var query2 = (from q in db.Qmimis
                          where q.CMIMIID == query
                          select q.Cmimi).FirstOrDefault();

            ViewBag.qmimi = query2;

            return View();
        }

        [HttpPost]
        [ActionName("RegjistroDN2")]
        public ActionResult RegjistroDN2Post(DergesatNderkombetare dn1)
        {
            int id = GetId();
            int query1 = (from s in db.Stafis
                          where s.UserID == id
                          select s.ZpID).FirstOrDefault();


            int query = (from q in db.Qmimis
                         where q.SherbimiId == dn1.LlojiSherbimitId && q.PeshaId == dn1.PeshaId && q.Trafiku == "Nderkombetare"
                         select q.CMIMIID).FirstOrDefault();


            DergesatNderkombetare dn = new DergesatNderkombetare();
            try
            {
                dn.NrRekomandes = dn1.NrRekomandes;
                dn.LlojiSherbimitId = dn1.LlojiSherbimitId;
                dn.PeshaId = dn1.PeshaId;
                dn.ShtetiId = dn1.ShtetiId;
                dn.UserId = id;
                dn.Menyra = "Ajrore";
                dn.Derguesi = dn1.Derguesi;
                dn.AdresaDerguesit = dn1.AdresaDerguesit;
                dn.Marresi = dn1.Marresi;
                dn.AdresaMarresit = dn1.AdresaMarresit;
                dn.DataRegj = DateTime.Today;
                dn.QmimiId = query;
                dn.ZpId = query1;
                db.DergesatNderkombetares.Add(dn);
                db.SaveChanges();

                return RedirectToAction("ListaDN");
            }
            catch (DbEntityValidationException ex)
            {
                var errorMessages = ex.EntityValidationErrors
                   .SelectMany(x => x.ValidationErrors)
                   .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);

            }

            //return View();
        }

        /////===========================================================================================================================

        [HttpGet]
        public ActionResult ListaDV(string kerko)
        {
            int id = GetId();
            List<DergesatVendore> dv = db.DergesatVendores.Where(x => x.UserId == id && x.DataRegj==DateTime.Today).ToList();
            if (kerko == null)
            {

                return View(dv);
            }
            else
            {

                var query1 = db.DergesatVendores.Where(a=>a.NrRekomandes.Contains(kerko) || a.LlojiSherbimit.EmriSherbimit.Contains(kerko)
                              || a.Peshat.Numri.Contains(kerko) || a.Userat.UserName.Contains(kerko)
                               || a.Qytetet.EmriQytetit.Contains(kerko) || a.Qmimi.Cmimi.ToString().Contains(kerko)
                                || a.Derguesi.Contains(kerko) || a.AdresaDerguesit.Contains(kerko)
                                 || a.Marresi.Contains(kerko) || a.AdresaMarresit.Contains(kerko)
                                  || a.DataRegj.ToString().Contains(kerko) || a.ZyraPostare.EmriZyres.Contains(kerko)).ToList();
                          

                return View(query1);

            }
         
            
        }
        [HttpGet]
        public ActionResult ListaDN(string kerko)
        {
            int id = GetId();
            List<DergesatNderkombetare> dn = db.DergesatNderkombetares.Where(x => x.UserId == id && x.DataRegj==DateTime.Today).ToList();


            if (kerko == null)
            {

                return View(dn);
            }
            else
            {

                var query1 = db.DergesatNderkombetares.Where(a => a.NrRekomandes.Contains(kerko) || a.LlojiSherbimit.EmriSherbimit.Contains(kerko)
                             || a.Peshat.Numri.Contains(kerko) || a.Userat.UserName.Contains(kerko)
                              || a.Shtetet.EmriShtetit.Contains(kerko) || a.Qmimi.Cmimi.ToString().Contains(kerko)
                               || a.Derguesi.Contains(kerko) || a.AdresaDerguesit.Contains(kerko)
                                || a.Marresi.Contains(kerko) || a.AdresaMarresit.Contains(kerko)
                                 || a.DataRegj.ToString().Contains(kerko) || a.ZyraPostare.EmriZyres.Contains(kerko) || a.Menyra.Contains(kerko)).ToList();
                            
                return View(query1);

            }
           
        }

      







    }
}