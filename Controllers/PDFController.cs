﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POSTA1.Models;
using System.IO;

namespace POSTA1.Controllers
{
    public class PDFController : Controller
    {
        public static PostaEntities7 db = new PostaEntities7();
        // GET: PDF




        public string RenderViewAsString(string viewName, object model)
        {
            // create a string writer to receive the HTML code
            StringWriter stringWriter = new StringWriter();

            // get the view to render
            ViewEngineResult viewResult = ViewEngines.Engines.FindView(ControllerContext, viewName, null);
            // create a context to render a view based on a model
            ViewContext viewContext = new ViewContext(
                    ControllerContext,
                    viewResult.View,
                    new ViewDataDictionary(model),
                    new TempDataDictionary(),
                    stringWriter
                    );

            // render the view to a HTML code
            viewResult.View.Render(viewContext, stringWriter);

            // return the HTML code
            return stringWriter.ToString();
        }
        //==============================================Kontroller=================================================================//

        public ActionResult KonPdfDV(List<DergesatVendore> model, string viewname)
        {

            var htmlContent = RenderViewAsString(viewname, model);
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            var pdfBuffer = htmlToPdf.GeneratePdf(htmlContent);
            // send the PDF file to browser
            FileResult fileResult = new FileContentResult(pdfBuffer, "application/pdf");
            fileResult.FileDownloadName = "PDF.pdf";

            return fileResult;
        }

        public ActionResult KontrolleriDV(List<DergesatVendore> model)
        {

            return View(model);
        }

        public ActionResult KonPdfDN(List<DergesatNderkombetare> model, string viewname)
        {

            var htmlContent = RenderViewAsString(viewname, model);
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            var pdfBuffer = htmlToPdf.GeneratePdf(htmlContent);
            // send the PDF file to browser
            FileResult fileResult = new FileContentResult(pdfBuffer, "application/pdf");
            fileResult.FileDownloadName = "PDF.pdf";

            return fileResult;
        }


        //public ActionResult KontrolleriDN(List<DergesatNderkombetare> dn)
        //{
        //    return View(dn);
        //}

        public ActionResult KonKerkesatPDF(List<Kerkesat> model, string viewname)
        {

            var htmlContent = RenderViewAsString(viewname, model);
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            var pdfBuffer = htmlToPdf.GeneratePdf(htmlContent);
            // send the PDF file to browser
            FileResult fileResult = new FileContentResult(pdfBuffer, "application/pdf");
            fileResult.FileDownloadName = "PDF.pdf";

            return fileResult;
        }


        public ActionResult KonKerkesat(List<Kerkesat> model)
        {
            return View(model);
        }

        public ActionResult KonPergjigjjetPDF(List<PergjigjjaKerkes> model, string viewname)
        {

            var htmlContent = RenderViewAsString(viewname, model);
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            var pdfBuffer = htmlToPdf.GeneratePdf(htmlContent);
            // send the PDF file to browser
            FileResult fileResult = new FileContentResult(pdfBuffer, "application/pdf");
            fileResult.FileDownloadName = "PDF.pdf";

            return fileResult;
        }


        public ActionResult KonPergjigjjet(List<PergjigjjaKerkes> model)
        {
            return View(model);
        }




        //==============================================QTP=================================================================//
        public ActionResult QtpPdfPostaPranuar(List<DergesatVendore> model, string viewname)
        {

            var htmlContent = RenderViewAsString(viewname, model);
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            var pdfBuffer = htmlToPdf.GeneratePdf(htmlContent);
            // send the PDF file to browser
            FileResult fileResult = new FileContentResult(pdfBuffer, "application/pdf");
            fileResult.FileDownloadName = "PDF.pdf";

            return fileResult;
        }

        public ActionResult QtpPostaPranuar(List<DergesatVendore> model)
        {

            return View(model);
        }
        public ActionResult QtpPdfPostaNgarkuar(List<DergesatPerDorezim> model, string viewname)
        {

            var htmlContent = RenderViewAsString(viewname, model);
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            var pdfBuffer = htmlToPdf.GeneratePdf(htmlContent);
            // send the PDF file to browser
            FileResult fileResult = new FileContentResult(pdfBuffer, "application/pdf");
            fileResult.FileDownloadName = "PDF.pdf";

            return fileResult;
        }

        public ActionResult QtpPostaNgarkuar(List<DergesatPerDorezim> model)
        {

            return View(model);
        }



        public ActionResult QtpPdfListaDK(List<DergesatDorezuara> model, string viewname)
        {

            var htmlContent = RenderViewAsString(viewname, model);
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            var pdfBuffer = htmlToPdf.GeneratePdf(htmlContent);
            // send the PDF file to browser
            FileResult fileResult = new FileContentResult(pdfBuffer, "application/pdf");
            fileResult.FileDownloadName = "PDF.pdf";

            return fileResult;
        }

        public ActionResult QtpListaDK(List<DergesatDorezuara> model)
        {

            return View(model);
        }

        public ActionResult QtpPdfDN(List<DergesatNderkombetare> model, string viewname)
        {

            var htmlContent = RenderViewAsString(viewname, model);
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            var pdfBuffer = htmlToPdf.GeneratePdf(htmlContent);
            // send the PDF file to browser
            FileResult fileResult = new FileContentResult(pdfBuffer, "application/pdf");
            fileResult.FileDownloadName = "PDF.pdf";

            return fileResult;
        }

        public ActionResult QtpDN(List<DergesatNderkombetare> model)
        {

            return View(model);
        }


        public ActionResult QtpPdfTransport(List<Transporti> model, string viewname)
        {

            var htmlContent = RenderViewAsString(viewname, model);
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            var pdfBuffer = htmlToPdf.GeneratePdf(htmlContent);
            // send the PDF file to browser
            FileResult fileResult = new FileContentResult(pdfBuffer, "application/pdf");
            fileResult.FileDownloadName = "PDF.pdf";

            return fileResult;
        }

        public ActionResult QtpTransport(List<Transporti> model)
        {

            return View(model);
        }


        public ActionResult QtpPdfKerkesat(List<Kerkesat> model, string viewname)
        {

            var htmlContent = RenderViewAsString(viewname, model);
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            var pdfBuffer = htmlToPdf.GeneratePdf(htmlContent);
            // send the PDF file to browser
            FileResult fileResult = new FileContentResult(pdfBuffer, "application/pdf");
            fileResult.FileDownloadName = "PDF.pdf";

            return fileResult;
        }

        public ActionResult QtpKerkesat(List<Kerkesat> model)
        {

            return View(model);
        }

        public ActionResult QtpPdfPergjigjjet(List<PergjigjjaKerkes> model, string viewname)
        {

            var htmlContent = RenderViewAsString(viewname, model);
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            var pdfBuffer = htmlToPdf.GeneratePdf(htmlContent);
            // send the PDF file to browser
            FileResult fileResult = new FileContentResult(pdfBuffer, "application/pdf");
            fileResult.FileDownloadName = "PDF.pdf";

            return fileResult;
        }

        public ActionResult QtpPergjigjjet(List<PergjigjjaKerkes> model)
        {

            return View(model);
        }



        //==============================================Postier=================================================================//



        public ActionResult PosPdfDorezim(List<DergesatPerDorezim> model, string viewname)
        {

            var htmlContent = RenderViewAsString(viewname, model);
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            var pdfBuffer = htmlToPdf.GeneratePdf(htmlContent);
            // send the PDF file to browser
            FileResult fileResult = new FileContentResult(pdfBuffer, "application/pdf");
            fileResult.FileDownloadName = "PDF.pdf";

            return fileResult;
        }

        public ActionResult PosDorezim(List<DergesatPerDorezim> model)
        {

            return View(model);
        }

        public ActionResult PosPdfListaDK(List<DergesatDorezuara> model, string viewname)
        {

            var htmlContent = RenderViewAsString(viewname, model);
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            var pdfBuffer = htmlToPdf.GeneratePdf(htmlContent);
            // send the PDF file to browser
            FileResult fileResult = new FileContentResult(pdfBuffer, "application/pdf");
            fileResult.FileDownloadName = "PDF.pdf";

            return fileResult;
        }

        public ActionResult PosListaDK(List<DergesatDorezuara> model)
        {

            return View(model);
        }

        public ActionResult PosPdfDnDorezim(List<Transporti> model, string viewname)
        {

            var htmlContent = RenderViewAsString(viewname, model);
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            var pdfBuffer = htmlToPdf.GeneratePdf(htmlContent);
            // send the PDF file to browser
            FileResult fileResult = new FileContentResult(pdfBuffer, "application/pdf");
            fileResult.FileDownloadName = "PDF.pdf";

            return fileResult;
        }

        public ActionResult PosDnDorezim(List<Transporti> model)
        {

            return View(model);
        }




        //==============================================Sportelist=================================================================//

        public ActionResult SportelPDFDV(List<DergesatVendore> model, string viewname)
        {

            var htmlContent = RenderViewAsString(viewname, model);
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            var pdfBuffer = htmlToPdf.GeneratePdf(htmlContent);
            // send the PDF file to browser
            FileResult fileResult = new FileContentResult(pdfBuffer, "application/pdf");
            fileResult.FileDownloadName = "PDF.pdf";

            return fileResult;
        }

        public ActionResult SportelDV(List<DergesatVendore> model)
        {

            return View(model);
        }
        public ActionResult SportelPDFDN(List<DergesatNderkombetare> model, string viewname)
        {

            var htmlContent = RenderViewAsString(viewname, model);
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            var pdfBuffer = htmlToPdf.GeneratePdf(htmlContent);
            // send the PDF file to browser
            FileResult fileResult = new FileContentResult(pdfBuffer, "application/pdf");
            fileResult.FileDownloadName = "PDF.pdf";

            return fileResult;
        }

        public ActionResult SportelDN(List<DergesatNderkombetare> model)
        {

            return View(model);
        }
    }
}