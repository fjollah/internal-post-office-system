﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using POSTA1.Models;
using Rotativa;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;


namespace POSTA1.Controllers
{
    [Authorize(Roles = "Kontroller")]
    public class KontrolleriController : Controller
    {
        public static PostaEntities7 db = new PostaEntities7();



        public int GetId()
        {
            var emri = User.Identity.Name;

            var query = from f in db.Userats
                        where f.UserName == emri
                        select f;

            int id2 = Convert.ToInt32(query.FirstOrDefault().UID);
            return id2;
        }

        public int GetSid()
        {
            var emri = User.Identity.Name;

            var query = from f in db.Userats
                        where f.UserName == emri
                        select f;

            int id2 = Convert.ToInt32(query.FirstOrDefault().UID);
            var user = from q in db.Stafis
                       where q.UserID == id2
                       select q;
            int id1 = Convert.ToInt32(user.FirstOrDefault().SID);
            return id1;



        }
        // GET: Admin
        [HttpGet]
        public ActionResult Index()
        {


            int sid = GetSid();

            Stafi st = db.Stafis.Where(x => x.SID == sid).FirstOrDefault();
            return View(st);




        }
        [HttpPost]
        public ActionResult Index(string pass, string passIRI)
        {
            int id = GetId();

            Stafi st = db.Stafis.FirstOrDefault(a => a.UserID == id);


            if (pass == passIRI)
            {
                Userat u = (from x in db.Userats
                            where x.UID == id
                            select x).First();
                u.Fjalekalimi = pass;

                db.SaveChanges();

            }
            else
            {
                TempData["Message"] = "Fjalekalimet duhet te jene te njejt";
            }

            return View(st);
        }



        /////===========================================================================================================================

        [HttpGet]
        public ActionResult DergesatVendore(string kerko)
        {

            int id = GetId();
            var query = (from x in db.Stafis
                         where x.UserID == id
                         select x.ZpID).First();

            List<DergesatVendore> DV = db.DergesatVendores.Where(x => x.ZpId == query && x.DataRegj == DateTime.Today).ToList();
            if (kerko == null)
            {
                ViewData["rez"] = DV;
                return View(DV);
            }
            else
            {

                var query1 = db.DergesatVendores.Where(a => a.NrRekomandes.Contains(kerko) || a.LlojiSherbimit.EmriSherbimit.Contains(kerko)
                             || a.Peshat.Numri.Contains(kerko) || a.Userat.UserName.Contains(kerko)
                              || a.Qytetet.EmriQytetit.Contains(kerko) || a.Qmimi.Cmimi.ToString().Contains(kerko)
                               || a.Derguesi.Contains(kerko) || a.AdresaDerguesit.Contains(kerko)
                                || a.Marresi.Contains(kerko) || a.AdresaMarresit.Contains(kerko)
                                 || a.DataRegj.ToString().Contains(kerko) || a.ZyraPostare.EmriZyres.Contains(kerko)).ToList();

                ViewData["rez"] = DV;
                return View(query1);

            }







        }








        [HttpGet]
        public ActionResult DergesatNderkombetare(string kerko)
        {
            int id = GetId();

            var query = (from x in db.Stafis
                         where x.UserID == id
                         select x.ZpID).First();

            List<DergesatNderkombetare> Dn = db.DergesatNderkombetares.Where(x => x.ZpId == query && x.DataRegj == DateTime.Today).ToList();
            if (kerko == null)
            {

                return View(Dn);
            }
            else
            {

                var query1 = db.DergesatNderkombetares.Where(a => a.NrRekomandes.Contains(kerko) || a.LlojiSherbimit.EmriSherbimit.Contains(kerko)
                             || a.Peshat.Numri.Contains(kerko) || a.Userat.UserName.Contains(kerko)
                              || a.Shtetet.EmriShtetit.Contains(kerko) || a.Qmimi.Cmimi.ToString().Contains(kerko)
                               || a.Derguesi.Contains(kerko) || a.AdresaDerguesit.Contains(kerko)
                                || a.Marresi.Contains(kerko) || a.AdresaMarresit.Contains(kerko)
                                 || a.DataRegj.ToString().Contains(kerko) || a.ZyraPostare.EmriZyres.Contains(kerko) || a.Menyra.Contains(kerko)).ToList();


                return View(query1);

            }



        }
        [HttpGet]
        public ActionResult DergoKerkese()
        {
            int id = GetId();
            return View();
        }
        [HttpPost]
        public ActionResult DergoKerkese(Kerkesat k1)
        {
            int id = GetId();
            Kerkesat k = new Kerkesat();
            k.KERKESAID = k1.KERKESAID;
            k.UserId = id;
            k.Data = DateTime.Today;
            k.Pershkrimi = k1.Pershkrimi;
            db.Kerkesats.Add(k);
            db.SaveChanges();
            return RedirectToAction("KerkesatDerguara");
        }

        [HttpGet]
        public ActionResult KerkesatDerguara(string kerko)
        {
            int id = GetId();
            List<Kerkesat> k = db.Kerkesats.Where(x => x.UserId == id).ToList();
            if (kerko == null)
            {

                return View(k);
            }
            else
            {

                var query1 = from a in k
                             where (a.Userat.UserName.Contains(kerko) || a.Pershkrimi.Contains(kerko) || a.Data.ToString().Contains(kerko))
                             select a;

                return View(query1.ToList());

            }


        }
        [HttpGet]
        public ActionResult Pergjigjjet(string kerko)
        {
            int id = GetId();
            List<PergjigjjaKerkes> pk = db.PergjigjjaKerkeses.Where(x => x.MarresiPergjigjjes == id).ToList();
            if (kerko == null)
            {

                return View(pk);
            }
            else
            {

                var query1 = from a in pk
                             where (a.Userat.UserName.Contains(kerko) || a.Pershkrimi.Contains(kerko) || a.Data.ToString().Contains(kerko))
                             select a;

                return View(query1.ToList());

            }


        }

      


    }
}