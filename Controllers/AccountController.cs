﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using POSTA1.Models;
using System.Web.Security;
using Rotativa;
using System.Collections.Generic;
using NReco;
using System.IO;


namespace POSTA1.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        public static PostaEntities7 db = new PostaEntities7();
        //===========================================pdf faqet==========================================================================

        //===================================================pdf========================================================================



        [AllowAnonymous]
        //ngarkimi dergesat vendore
        public ActionResult pdfDorezim(int ID)
        {
            var pdf = db.DergesatDorezuaras.Where(x => x.DDid == ID).FirstOrDefault();

            return View(pdf);

        }
        [AllowAnonymous]
        public ActionResult DVpdfDorezim(int id)
        {

            return new ActionAsPdf("pdfDorezim", new { id = id });


        }

        [AllowAnonymous]
        // transporti kerkesat nderkombetare
        public ActionResult pdfTransport(int ID)
        {
            var pdf = db.Transportis.Where(x => x.TRANSPORTID == ID).FirstOrDefault();

            return View(pdf);

        }
        [AllowAnonymous]
        public ActionResult DNpdfDorezim(int id)
        {

            return new ActionAsPdf("pdfTransport", new { id = id });


        }
        [AllowAnonymous]
        //pergjigjjet e kerkesave
        public ActionResult pdfPergjigjjet(int ID)
        {
            var pdf = db.PergjigjjaKerkeses.Where(x => x.PKerkesesID == ID).FirstOrDefault();


            return View(pdf);

        }
        [AllowAnonymous]
        public ActionResult pdfPergjigj(int id)
        {

            return new ActionAsPdf("pdfPergjigjjet", new { id = id });


        }

        [AllowAnonymous]
        //kerkesat
        public ActionResult pdfKerkesat(int ID)
        {
            var pdf = db.Kerkesats.Where(x => x.KERKESAID == ID).FirstOrDefault();


            return View(pdf);

        }
        [AllowAnonymous]
        public ActionResult pdfKerkese(int id)
        {

            return new ActionAsPdf("pdfKerkesat", new { id = id });


        }

        [AllowAnonymous]
        //dergesat vendore
        public ActionResult DVPDF(int ID)
        {
            var pdf = db.DergesatVendores.Where(x => x.DVID == ID).FirstOrDefault();

            return View(pdf);

        }
        [AllowAnonymous]
        public ActionResult pdf(int id)
        {

            return new ActionAsPdf("DVPDF", new { id = id });


        }

        [AllowAnonymous]
        //dergesat Nderkombetare
        public ActionResult DNPDF(int ID)
        {
            var pdf = db.DergesatNderkombetares.Where(x => x.DNID == ID).FirstOrDefault();

            return View(pdf);

        }
        [AllowAnonymous]
        public ActionResult pdfDN(int id)
        {

            return new ActionAsPdf("DNPDF", new { id = id });


        }

        [AllowAnonymous]
        //dergesat Nderkombetare
        public ActionResult DvPerDorezim(int ID)
        {
            var pdf = db.DergesatPerDorezims.Where(x => x.DDID == ID).FirstOrDefault();

            return View(pdf);

        }
        [AllowAnonymous]
        public ActionResult DvPerDorezim1(int id)
        {

            return new ActionAsPdf("DvPerDorezim", new { id = id });


        }






        //==========================================================================================================================




   

        
        [AllowAnonymous]
        public ActionResult Login()
        {
            
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Userat model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
                           var UserData = from ANU in db.Userats
                           where ANU.UserName == model.UserName
                           select ANU;
                           foreach (var s in UserData)
                           {
                               var user = from d in db.Stafis
                                          where s.UID == d.UserID
                                          select d.SID;

                               if (string.IsNullOrEmpty(s.Fjalekalimi))
                                   ModelState.AddModelError("", "Te dhenat jane gabim!.");
                               else
                               {
                                   if (s.Fjalekalimi.Equals(model.Fjalekalimi))
                                   {
                                       switch (s.GrupiID)
                                       {
                                           case 1:
                                               {
                                                   FormsAuthentication.SetAuthCookie(model.UserName, false);
                                                   return RedirectToAction("Index", "Admin");
                                               }
                                           case 2:
                                               {
                                                   FormsAuthentication.SetAuthCookie(model.UserName, false);
                                                   return RedirectToAction("Index", "QTP");
                                               }
                                           case 3:
                                               {
                                                   FormsAuthentication.SetAuthCookie(model.UserName, false);
                                                   return RedirectToAction("Index", "Kontrolleri");

                                               }
                                           case 4:
                                               {
                                                   FormsAuthentication.SetAuthCookie(model.UserName, false);
                                                   return RedirectToAction("Index", "Sportelist");
                                               }
                                           case 5:
                                               {
                                                   FormsAuthentication.SetAuthCookie(model.UserName, false);
                                                   return RedirectToAction("Index", "Postieri");
                                               }
                                           default:
                                               break;
                                       }
                                   }
                                   else
                                   {
                                       ModelState.AddModelError("", "Fjalekalimi eshte gabim.");
                                   }
                               }
                           }

           


                   
















             
            return View();
        }


        public ActionResult LogOut()
        {

            FormsAuthentication.SignOut();
            return RedirectToAction("Login","Account");
        }

        
        }
      //  #endregion
    }
