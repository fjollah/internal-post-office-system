﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POSTA1.Models;
using System.Web.Security;

namespace POSTA1.Controllers
{
    [Authorize(Roles="Postier")]
    public class PostieriController : Controller
    {
        public static PostaEntities7 db = new PostaEntities7();

        public int GetId()
        {
            var emri = User.Identity.Name;

            var query = from f in db.Userats
                        where f.UserName == emri
                        select f;

            int id2 = Convert.ToInt32(query.FirstOrDefault().UID);
            return id2;
        }

        public int GetSid()
        {
            var emri = User.Identity.Name;

            var query = from f in db.Userats
                        where f.UserName == emri
                        select f;

            int id2 = Convert.ToInt32(query.FirstOrDefault().UID);
            var user = from q in db.Stafis
                       where q.UserID == id2
                       select q;
            int id1 = Convert.ToInt32(user.FirstOrDefault().SID);
            return id1;



        }
        // GET: Admin
        [HttpGet]
        public ActionResult Index()
        {


            int sid = GetSid();

            Stafi st = db.Stafis.Where(x => x.SID == sid).FirstOrDefault();
            return View(st);




        }
        [HttpPost]
        public ActionResult Index(string pass, string passIRI)
        {
            int id = GetId();

            Stafi st = db.Stafis.FirstOrDefault(a => a.UserID == id);


            if (pass == passIRI)
            {
                Userat u = (from x in db.Userats
                            where x.UID == id
                            select x).First();
                u.Fjalekalimi = pass;

                db.SaveChanges();

            }
            else
            {
                TempData["Message"] = "Fjalekalimet duhet te jene te njejt";
            }

            return View(st);
        }

        /////===========================================================================================================================
        [HttpGet]
        public ActionResult DergesatDorezim(string kerko)
        {
            int id = GetId();




            List<DergesatPerDorezim> dp = db.DergesatPerDorezims.Where(x => x.PostieriId == id && x.Data==DateTime.Today).ToList();
            if (kerko == null)
            {
                var q1 = from c in dp
                         where !(from o in db.DergesatDorezuaras
                                 select o.DDid)
                                .Contains(c.DDID)
                         select c;

                return View(q1.ToList());
            }
            else
            {

                var query1 = db.DergesatPerDorezims.Where(a => a.Userat.UserName.Contains(kerko) || a.Pershkrimi.Contains(kerko) || a.Data.ToString().Contains(kerko) || a.DVendoreId.ToString().Contains(kerko)).ToList();
                var q1 = from c in query1
                         where !(from o in db.DergesatDorezuaras
                                 select o.DDid)
                                .Contains(c.DDID)
                         select c;


                return View(q1.ToList());

            }

        }
        [HttpGet]
        public ActionResult VerifikoDorezimin(int id)
        {
            ViewBag.ID = id;

            List<SelectListItem> items = new List<SelectListItem>();

            items.Add(new SelectListItem { Text = "Dorezuar", Value = "Dorezuar" });

            items.Add(new SelectListItem { Text = "Kthyer", Value = "Kthyer" });

            ViewBag.Aksioni = items;
            





            return View();


        }
        [HttpPost]
        public ActionResult VerifikoDorezimin(int id, DergesatDorezuara d1)
        {
            //useri qe eshte aktualisht i loguar (me session ose cookies)
            int id1 = GetId();



           DergesatDorezuara d = new DergesatDorezuara();
           d.DDid = id;
           d.PostieriId = id1;
            d.Data = DateTime.Today;
            d.Statusi = d1.Statusi;
            d.Pershkrimi = d1.Pershkrimi;
            db.DergesatDorezuaras.Add(d);

            db.SaveChanges();
            if(d.Statusi=="Dorezuar")
            {
                return RedirectToAction("DergesatDorezuara");
            }
            else
            {
                return RedirectToAction("DergesatKthyera");
            }

           
        }
        public ActionResult DergesatDorezuara(string kerko)
        {

            int id = GetId();
            List<DergesatDorezuara> dp = db.DergesatDorezuaras.Where(x => x.PostieriId==id && x.Statusi=="Dorezuar" && x.Data==DateTime.Today).ToList();

            if (kerko == null)
            {

                return View(dp);
            }
            else
            {

                var query1 = db.DergesatDorezuaras.Where(a=>a.Userat.UserName.Contains(kerko) || a.Pershkrimi.Contains(kerko) || a.Data.ToString().Contains(kerko) || a.Statusi.Contains(kerko)).ToList();

                var q = from a in query1
                        where a.Statusi == "Dorezuar"
                        select a;
                return View(q.ToList());

            }


           
        }
        public ActionResult DergesatKthyera(string kerko)
        {
            int id = GetId();
            List<DergesatDorezuara> dp = db.DergesatDorezuaras.Where(x => x.PostieriId == id && x.Statusi == "Kthyer" && x.Data == DateTime.Today).ToList();

            if (kerko == null)
            {

                return View(dp);
            }
            else
            {

                var query1 = db.DergesatDorezuaras.Where(a => a.Userat.UserName.Contains(kerko) || a.Pershkrimi.Contains(kerko) || a.Data.ToString().Contains(kerko) || a.Statusi.Contains(kerko)).ToList();
                var q = from a in query1
                        where a.Statusi == "Kthyer"
                        select a;

                return View(q.ToList());

            }
           
        }
        ///===========================================================================================================================
        public ActionResult DergesatNderkombetareDorezim(string kerko)
        {
            int id = GetId();


            List<Transporti> tn = db.Transportis.Where(x => x.PostieriId == id &&  x.Data==DateTime.Today).ToList();

            if (kerko == null)
            {

                return View(tn);
            }
            else
            {

                var query1 = db.Transportis.Where(a => a.Userat.UserName.Contains(kerko) || a.Pershkrimi.Contains(kerko) || a.Data.ToString().Contains(kerko) || a.Rrugetimi.Contains(kerko)).ToList();
                           
                return View(query1);

            }
         
        }

    }
}