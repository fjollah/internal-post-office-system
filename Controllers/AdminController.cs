﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POSTA1.Models;
using System.Web.Security;
using Microsoft.AspNet.Identity;


namespace POSTA1.Controllers
{
  [Authorize(Roles="Admin")]
    public class AdminController : Controller
    {
        public static PostaEntities7 db = new PostaEntities7();

       public int GetId()
        {
            var emri = User.Identity.Name;

            var query = from f in db.Userats
                        where f.UserName == emri
                        select f;

            int id2 = Convert.ToInt32(query.FirstOrDefault().UID);
            return id2;
        }
      
      public int GetSid()
        {
            var emri = User.Identity.Name;

            var query = from f in db.Userats
                        where f.UserName == emri
                        select f;

            int id2 = Convert.ToInt32(query.FirstOrDefault().UID);
            var user = from q in db.Stafis
                       where q.UserID == id2
                       select q;
            int id1 = Convert.ToInt32(user.FirstOrDefault().SID);
            return id1;
        }
        // GET: Admin
        [HttpGet]
        public ActionResult Index()
        {

            
            int sid = GetSid();

            Stafi st = db.Stafis.Where(x => x.SID == sid).FirstOrDefault();
                 return View(st);

            

           
        }
        [HttpPost]
        public ActionResult Index(string pass, string passIRI)
        {
            int id = GetId();
           
            Stafi st = db.Stafis.FirstOrDefault(a => a.UserID == id);
            

            if(pass==passIRI)
            {
                Userat u = (from x in db.Userats
                            where x.UID==id
                            select x).First();
                u.Fjalekalimi = pass;

                db.SaveChanges();

            }
            else
            {
                TempData["Message"] = "Fjalekalimet duhet te jene te njejt";
            }

            return View(st);
        }
        //=============================================ZYRA POSTARE=================================================================================

        public ActionResult ZPIndex(string kerko)
        {
            
            if (kerko != null)
            {

                var q = db.ZyraPostares.Where(a => a.EmriZyres.Contains(kerko) || a.Lokacioni.Contains(kerko) || a.Kontrolleri.Contains(kerko) || a.Qytetet.EmriQytetit.Contains(kerko)).ToList();
                return View(q);


            }
            else
            {
                return View(db.ZyraPostares.ToList());
            }







        }



        [HttpGet]
        public ActionResult ZPAdd()
        {
           
            ViewBag.Qytetet = new SelectList(db.Qytetets.ToList(), "QYTETIID", "EmriQytetit");

            return View();
        }

        [HttpPost]
        public ActionResult ZPAdd(ZyraPostare zp1, int Qytetet)
        {

            ZyraPostare zp = new ZyraPostare();

             bool query = db.ZyraPostares.Any(x => x.EmriZyres == zp1.EmriZyres);

             if (query == true)
             {
                 TempData["Message"] = "Ky emer i zyres ekziston! Provoni nje tjeter!";
                 ViewBag.Qytetet = new SelectList(db.Qytetets.ToList(), "QYTETIID", "EmriQytetit");
                 return View(zp1);

             }
             else
             {


                 zp.EmriZyres = zp1.EmriZyres;
                 zp.Lokacioni = zp1.Lokacioni;
                 zp.Kontrolleri = zp1.Kontrolleri;
                 zp.QytetiID = Qytetet;


                 db.ZyraPostares.Add(zp);

                 db.SaveChanges();


                 return RedirectToAction("ZPIndex");
             }
        }
        [HttpGet]
        public ActionResult ZPEdit(int id)
        {
          
            ViewBag.Qytetet = new SelectList(db.Qytetets.ToList(), "QYTETIID", "EmriQytetit");

            ZyraPostare zp = db.ZyraPostares.Where(a => a.ZPID == id).FirstOrDefault();

            db.SaveChanges();

            return View(zp);




        }

        [HttpPost]
        public ActionResult ZPEdit(ZyraPostare zp1)
        {
            
            ZyraPostare zp = db.ZyraPostares.Where(a => a.ZPID == zp1.ZPID).FirstOrDefault();
            bool query = db.ZyraPostares.Any(x => x.EmriZyres == zp1.EmriZyres && x.ZPID!=zp1.ZPID);

            if (query == true)
            {
                TempData["Message"] = "Ky emer i zyres ekziston! Provoni nje tjeter!";
                ViewBag.Qytetet = new SelectList(db.Qytetets.ToList(), "QYTETIID", "EmriQytetit");
                return View(zp1);

            }
            else
            {

                //zp.ZPID = zp1.ZPID;
                zp.EmriZyres = zp1.EmriZyres;
                zp.Lokacioni = zp1.Lokacioni;
                zp.Kontrolleri = zp1.Kontrolleri;
                zp.QytetiID = zp1.QytetiID;


                db.SaveChanges();

                return RedirectToAction("ZPIndex");

            }


        }

        public ActionResult ZPDelete(int id)
        {
            
            ZyraPostare zp1 = db.ZyraPostares.Where(a => a.ZPID == id).FirstOrDefault();
            Stafi st = db.Stafis.Where(a => a.ZpID == id).FirstOrDefault();
            if (st != null)
            {

                TempData["Message"] = "Zyra Postare nuk mund te fshihet sepse ka punetore te regjistruar ne te!";



            }
            else
            {


                db.ZyraPostares.Remove(zp1);
                db.SaveChanges();

            }



            return RedirectToAction("ZPIndex");
        }


        //-------------------------------------------------------USER---------------------------------------------------------------------

        public ActionResult USERIndex(string kerko)
        {
            
            ViewBag.Grupet = new SelectList(db.Grupets.ToList(), "GID", "EmriGrupit");

            if (kerko != null)
            {

                var q = db.Userats.Where(a => a.UserName.Contains(kerko) || a.Grupet.EmriGrupit.Contains(kerko)||a.UID.ToString().Contains(kerko)).ToList();
                return View(q);


            }
            else
            {
                return View(db.Userats.ToList());
            }







        }




        [HttpGet]
        public ActionResult USEREdit(int id)
        {
           
            ViewBag.Grupet = new SelectList(db.Grupets.ToList(), "GID", "EmriGrupit");

            Userat user = db.Userats.Where(a => a.UID == id).FirstOrDefault();

            db.SaveChanges();

            return View(user);




        }

        [HttpPost]
        public ActionResult USEREdit(Userat u1)
        {
            

            
            Userat u = db.Userats.Where(a => a.UID == u1.UID).FirstOrDefault();

            bool query = db.Userats.Any(x => x.UserName == u1.UserName && x.UID != u1.UID);

            if (query == true)
            {
                TempData["Message"] = "Ky emer i shfrytezuesit eshte i zene! Provoni nje tjeter!";
                ViewBag.Grupet = new SelectList(db.Grupets.ToList(), "GID", "EmriGrupit");
                return View(u1);

            }
            else
            {
                u.UID = u1.UID;
                u.UserName = u1.UserName;
                u.Fjalekalimi = u1.Fjalekalimi;
                u.GrupiID = u1.GrupiID;


                db.SaveChanges();

                return RedirectToAction("USERIndex");
            }




        }




        //=============================================================GRUPET=================================================================
        public ActionResult GRUPIndex(string kerko)
        {
            
            if (kerko != null)
            {

                var q = db.Grupets.Where(a => a.EmriGrupit.Contains(kerko)).ToList();
                return View(q);


            }
            else
            {
                return View(db.Grupets.ToList());
            }







        }

        [HttpGet]
        public ActionResult GRUPAdd()
        {
            

            return View();
        }

        [HttpPost]
        public ActionResult GRUPAdd(Grupet g1)
        {
           

            Grupet g = new Grupet();

            bool query = db.Grupets.Any(x => x.EmriGrupit == x.EmriGrupit);

            if (query == true)
            {
                TempData["Message"] = "Ky emer i grupit ekziston! Provoni nje tjeter!";

                return View(g1);

            }
            else
            {


                g.GID = g1.GID;
                g.EmriGrupit = g1.EmriGrupit;



                db.Grupets.Add(g);

                db.SaveChanges();


                return RedirectToAction("GRUPIndex");
            }
        }
        [HttpGet]
        public ActionResult GRUPEdit(int id)
        {
            
            
            Grupet g = db.Grupets.Where(a => a.GID == id).FirstOrDefault();
            

            db.SaveChanges();

            return View(g);




        }

        [HttpPost]
        public ActionResult GRUPEdit(Grupet g1)
        {
            
            
            Grupet g = db.Grupets.Where(a => a.GID == g1.GID).FirstOrDefault();
            bool query = db.Grupets.Any(x => x.EmriGrupit == g1.EmriGrupit && x.GID!=g1.GID);

            if (query == true)
            {
                TempData["Message"] = "Ky emer i grupit ekziston! Provoni nje tjeter!";

                return View(g1);

            }
            else
            {
                g.GID = g1.GID;
                g.EmriGrupit = g1.EmriGrupit;





                db.SaveChanges();

                return RedirectToAction("GRUPIndex");
            }




        }

        public ActionResult GRUPDelete(int id)
        {
           
            
            Grupet g = db.Grupets.Where(a => a.GID == id).FirstOrDefault();
            Userat u = db.Userats.Where(a => a.GrupiID == id).FirstOrDefault();
            if (u != null)
            {

                TempData["Message"] = "Grupi nuk mund te fshihet sepse ka punetore te regjistruar ne te!";



            }
            else
            {


                db.Grupets.Remove(g);
                db.SaveChanges();

            }



            return RedirectToAction("GRUPIndex");
        }

        //=============================================================SHTETET=================================================================
        public ActionResult SHTETIndex(string kerko)
        {
           
            if (kerko != null)
            {

                var q = db.Shtetets.Where(a => a.EmriShtetit.Contains(kerko)).ToList();
                return View(q);


            }
            else
            {
                return View(db.Shtetets.ToList());
            }







        }

        [HttpGet]
        public ActionResult SHTETAdd()
        {
           


            return View();
        }

        [HttpPost]
        public ActionResult SHTETAdd(Shtetet sh1)
        {
            
            Shtetet sh = new Shtetet();
            bool query = db.Shtetets.Any(x => x.EmriShtetit == sh1.EmriShtetit);

            if (query == true)
            {
                TempData["Message"] = "Ky shtet ekziston ne liste! Provoni nje tjeter!";

                return View(sh1);

            }
            else
            {
                sh.SHTETIID = sh1.SHTETIID;
                sh.EmriShtetit = sh1.EmriShtetit;



                db.Shtetets.Add(sh);

                db.SaveChanges();


                return RedirectToAction("SHTETIndex");
            }
        }
        [HttpGet]
        public ActionResult SHTETEdit(int id)
        {
            Shtetet sh = db.Shtetets.Where(a => a.SHTETIID == id).FirstOrDefault();

            db.SaveChanges();

            return View(sh);




        }

        [HttpPost]
        public ActionResult SHTETEdit(Shtetet sh1)
        {
            
            Shtetet sh = db.Shtetets.Where(a => a.SHTETIID == sh1.SHTETIID).FirstOrDefault();
             bool query = db.Shtetets.Any(x => x.EmriShtetit == sh1.EmriShtetit && x.SHTETIID!=sh1.SHTETIID);

             if (query == true)
             {
                 TempData["Message"] = "Ky shtet ekziston ne liste! Provoni nje tjeter!";

                 return View(sh1);

             }
             else
             {
                 sh.SHTETIID = sh1.SHTETIID;
                 sh.EmriShtetit = sh1.EmriShtetit;





                 db.SaveChanges();

                 return RedirectToAction("SHTETIndex");
             }



        }

        public ActionResult SHTETDelete(int id)
        {
            
            Shtetet sh = db.Shtetets.Where(a => a.SHTETIID == id).FirstOrDefault();
            DergesatNderkombetare dn = db.DergesatNderkombetares.Where(a => a.ShtetiId == id).FirstOrDefault();
            if (dn != null)
            {

                TempData["Message"] = "Shteti  nuk mund te fshihet sepse ka dergesa te regjistruara ne te!";



            }
            else
            {


                db.Shtetets.Remove(sh);
                db.SaveChanges();

            }



            return RedirectToAction("SHTETIndex");
        }



        //=============================================================QYTETET=================================================================
        public ActionResult QYTETIndex(string kerko)
        {
            

            if (kerko != null)
            {

                var q = db.Qytetets.Where(a => a.EmriQytetit.Contains(kerko)).ToList();
                return View(q);


            }
            else
            {
                return View(db.Qytetets.ToList());
            }







        }

        [HttpGet]
        public ActionResult QYTETAdd()
        {

            return View();
        }

        [HttpPost]
        public ActionResult QYTETAdd(Qytetet q1)
        {
            Qytetet q = new Qytetet();
            
             bool query = db.Qytetets.Any(x => x.EmriQytetit == q1.EmriQytetit);

             if (query == true)
             {
                 TempData["Message"] = "Ky qytet ekziston ne liste! Provoni nje tjeter!";

                 return View(q1);

             }
             else
             {

                 q.QYTETIID = q1.QYTETIID;
                 q.EmriQytetit = q1.EmriQytetit;


                 db.Qytetets.Add(q);

                 db.SaveChanges();


                 return RedirectToAction("QYTETIndex");
             }
        }
        [HttpGet]
        public ActionResult QYTETEdit(int id)
        {
            Qytetet q = db.Qytetets.Where(a => a.QYTETIID == id).FirstOrDefault();

            db.SaveChanges();

            return View(q);




        }

        [HttpPost]
        public ActionResult QYTETEdit(Qytetet q1)
        {
            Qytetet q = db.Qytetets.Where(a => a.QYTETIID == q1.QYTETIID).FirstOrDefault();
             bool query = db.Qytetets.Any(x => x.EmriQytetit == q1.EmriQytetit && x.QYTETIID!=q1.QYTETIID);

             if (query == true)
             {
                 TempData["Message"] = "Ky qytet ekziston ne liste! Provoni nje tjeter!";

                 return View(q1);

             }
             else
             {
                 q.QYTETIID = q1.QYTETIID;
                 q.EmriQytetit = q1.EmriQytetit;





                 db.SaveChanges();

                 return RedirectToAction("QYTETIndex");
             }



        }

        public ActionResult QYTETDelete(int id)
        {
            
            
            Qytetet q = db.Qytetets.Where(a => a.QYTETIID == id).FirstOrDefault();
            DergesatVendore dv = db.DergesatVendores.Where(a => a.QytetiId == id).FirstOrDefault();
            ZyraPostare zp = db.ZyraPostares.Where(a => a.QytetiID == id).FirstOrDefault();
            if (dv != null || zp != null)
            {

                TempData["Message"] = "Qyteti  nuk mund te fshihet sepse ka dergesa dhe zyra postare te regjistruara ne te!";



            }
            else
            {


                db.Qytetets.Remove(q);
                db.SaveChanges();

            }



            return RedirectToAction("QYTETIndex");
        }

        //=============================================STAFI=================================================================================

        public ActionResult STAFIndex(string kerko)
        {
            
            ViewBag.Zyrat = new SelectList(db.ZyraPostares.ToList(), "ZPID", "EmriZyres");
            ViewBag.Userat = new SelectList(db.Userats.ToList(), "UID", "UserName");

            if (kerko != null)
            {

                var q = db.Stafis.Where(a => a.Emri.Contains(kerko) || a.NrPersonal.ToString().Contains(kerko) || a.Pozita.Contains(kerko) ||
                    a.Telefoni.Contains(kerko) || a.Userat.UserName.Contains(kerko) || a.VendiPunes.Contains(kerko) || a.ZyraPostare.EmriZyres.Contains(kerko) || a.Adresa.Contains(kerko) ||
                    a.Email.ToString().Contains(kerko) || a.Ditelindja.ToString().Contains(kerko) || a.DataFillimit.ToString().Contains(kerko) || a.DataMbarimit.ToString().Contains(kerko)).ToList();
                return View(q);


            }
            else
            {



                return View(db.Stafis.ToList());
            }





        }



        [HttpGet]
        public ActionResult STAFAdd()
        {
            
            ViewBag.Zyrat = new SelectList(db.ZyraPostares.ToList(), "ZPID", "EmriZyres");
            ViewBag.Grupet = new SelectList(db.Grupets.ToList(), "GID", "EmriGrupit");



            return View();
        }

        [HttpPost]
        public ActionResult STAFAdd(Stafi s1, int Zyrat,int Grupet)
        {
           
            
            s1.ZpID = Zyrat;
            s1.Userat.GrupiID = Grupet;

        
            bool query = db.Userats.Any(x => x.UserName == s1.Userat.UserName);

            if (query == true)
            {
                TempData["Message"] = "Ky emer i shfrytezuesit eshte i zene! Provoni nje tjeter!";
                ViewBag.Zyrat = new SelectList(db.ZyraPostares.ToList(), "ZPID", "EmriZyres");
                ViewBag.Grupet = new SelectList(db.Grupets.ToList(), "GID", "EmriGrupit");
                return View(s1);

            }

            else
            {
                Stafi s = new Stafi();
                Userat u = new Userat();

                s.Emri = s1.Emri;
                s.Adresa = s1.Adresa;
                s.DataFillimit = s1.DataFillimit;
                s.DataMbarimit = s1.DataMbarimit;
                s.Ditelindja = s1.Ditelindja;
                s.Email = s1.Email;
                s.Telefoni = s1.Telefoni;
                s.NrPersonal = s1.NrPersonal;
                s.Pozita = s1.Pozita;
                s.ZpID = s1.ZpID;
                s.VendiPunes = s1.VendiPunes;

                u.UserName = s1.Userat.UserName;
                u.Fjalekalimi = s1.Userat.Fjalekalimi;
                u.GrupiID = s1.Userat.GrupiID;


                db.Stafis.Add(s);
                db.Userats.Add(u);

                db.SaveChanges();


                return RedirectToAction("STAFIndex");
            }
            
          

        }
        [HttpGet]
        public ActionResult STAFEdit(int id)
        {
            
            ViewBag.Zyrat = new SelectList(db.ZyraPostares.ToList(), "ZPID", "EmriZyres");
            ViewBag.Grupet = new SelectList(db.Grupets.ToList(), "GID", "EmriGrupit");




            Stafi st = db.Stafis.Where(a => a.SID == id).FirstOrDefault();

            db.SaveChanges();

            return View(st);




        }

        [HttpPost]
        public ActionResult STAFEdit(int id, Stafi s1)
        {
            
            

            if (ModelState.IsValid) //this condition is for validation checking on server side
            {
                Stafi s = db.Stafis.Where(a => a.SID == id).FirstOrDefault();
                Userat u = new Userat();

                s.Emri = s1.Emri;
                s.Adresa = s1.Adresa;
                s.NrPersonal = s1.NrPersonal;
                s.DataFillimit = s1.DataFillimit;
                s.DataMbarimit = s1.DataMbarimit;
                s.Ditelindja = s1.Ditelindja;
                s.Email = s1.Email;
                s.Telefoni = s1.Telefoni;
                s.NrPersonal = s1.NrPersonal;
                s.Pozita = s1.Pozita;
                s.ZpID = s1.ZpID;
                s.VendiPunes = s1.VendiPunes;

                u = db.Userats.Where(a => a.UID == s.UserID).FirstOrDefault();




                u.UserName = s1.Userat.UserName;
                u.Fjalekalimi = s1.Userat.Fjalekalimi;
                u.GrupiID = s1.Userat.GrupiID;






                db.SaveChanges();

                return RedirectToAction("STAFIndex");
            }
            else
            {

                ViewBag.Zyrat = new SelectList(db.ZyraPostares.ToList(), "ZPID", "EmriZyres");
                ViewBag.Grupet = new SelectList(db.Grupets.ToList(), "GID", "EmriGrupit");


                return View(s1);
            }




        }






        public ActionResult STAFDelete(int id)
        {
            
            Stafi st = db.Stafis.Include("Userat").Where(a => a.SID == id).FirstOrDefault();
            Userat u = db.Userats.Where(a => a.UID == st.UserID).FirstOrDefault();

            DergesatNderkombetare dv = db.DergesatNderkombetares.Where(a => a.UserId == u.UID).FirstOrDefault();
            DergesatVendore dn = db.DergesatVendores.Where(a => a.UserId == u.UID).FirstOrDefault();
            DergesatPerDorezim dd = db.DergesatPerDorezims.Where(a => a.UserID == u.UID).FirstOrDefault();
            DergesatDorezuara dd1 = db.DergesatDorezuaras.Where(a => a.PostieriId == u.UID).FirstOrDefault();

            if (dv != null && dn!=null && dd!=null && dd1!=null)
            {
                TempData["Message"] = "Ky perdorues nuk mund te fshihet sepse ka regjistruar dhe ngarkuar letra!";

            }
            else
            {
                db.Stafis.Remove(st);
                db.Userats.Remove(u);
                db.SaveChanges();

            }

            return RedirectToAction("STAFIndex");


        }



        //=============================================================SHERBIMET=================================================================
        public ActionResult SHERBIMIndex(string kerko)
        {
            if (kerko != null)
            {

                var q = db.LlojiSherbimits.Where(a => a.EmriSherbimit.Contains(kerko) || a.Pershkrimi.Contains(kerko)).ToList();
                return View(q);


            }
            else
            {
                return View(db.LlojiSherbimits.ToList());
            }







        }

        [HttpGet]
        public ActionResult SHERBIMIAdd()
        {
            
            LlojiSherbimit ll = new LlojiSherbimit();
            


            return View();
        }

        [HttpPost]
        public ActionResult SHERBIMIAdd(LlojiSherbimit ll1)
        {
           
            

            LlojiSherbimit ll = new LlojiSherbimit();

             bool query = db.LlojiSherbimits.Any(x => x.EmriSherbimit == ll1.EmriSherbimit);


             if (query == true)
             {
                 TempData["Message"] = "Ky sherbim ekziston! Provoni nje tjeter!";

                 return View(ll1);

             }
             else
             {
                 ll.EmriSherbimit = ll1.EmriSherbimit;
                 ll.Pershkrimi = ll1.Pershkrimi;
                 ll.Prioriteti = ll1.Prioriteti;



                 db.LlojiSherbimits.Add(ll);

                 db.SaveChanges();


                 return RedirectToAction("SHERBIMIndex");
             }
        }
        [HttpGet]
        public ActionResult SHERBIMIEdit(int id)
        {
            
            LlojiSherbimit ll = db.LlojiSherbimits.Where(a => a.SHERBIMIID == id).FirstOrDefault();

            db.SaveChanges();

            return View(ll);




        }

        [HttpPost]
        public ActionResult SHERBIMIEdit(LlojiSherbimit ll1)
        {
            
            LlojiSherbimit ll = db.LlojiSherbimits.Where(a => a.SHERBIMIID == ll1.SHERBIMIID).FirstOrDefault();
            bool query = db.LlojiSherbimits.Any(x => x.EmriSherbimit == ll1.EmriSherbimit && x.SHERBIMIID != ll1.SHERBIMIID);

            if (query == true)
            {
                TempData["Message"] = "Ky sherbim ekziston! Provoni nje tjeter!";

                return View(ll1);

            }
            else
            {
                ll.EmriSherbimit = ll1.EmriSherbimit;
                ll.Pershkrimi = ll1.Pershkrimi;
                ll.Prioriteti = ll1.Prioriteti;


                db.SaveChanges();

                return RedirectToAction("SHERBIMIndex");




            }
        }
        public ActionResult SHERBIMIDelete(int id)
        {
            
            LlojiSherbimit ll = db.LlojiSherbimits.Where(a => a.SHERBIMIID == id).FirstOrDefault();

            Qmimi q = db.Qmimis.Where(a => a.SherbimiId == ll.SHERBIMIID).FirstOrDefault();


            if (q != null)
            {

                TempData["Message"] = "Lloji i Sherbimit  nuk mund te fshihet sepse ka dergesa  te regjistruara ne te!";



            }
            else
            {


                db.LlojiSherbimits.Remove(ll);
                db.SaveChanges();

            }



            return RedirectToAction("SHERBIMIndex");
        }


        //=============================================================TRAFIKU(VENDOR DHE NDERKOMBETAR)=================================================================




        //=============================================================PESHAT=================================================================
        public ActionResult PESHAIndex(string kerko)
        {

            if (kerko != null)
            {

                var q = db.Peshats.Where(a => a.NjesiaMatese.Contains(kerko) || a.Numri.ToString().Contains(kerko)).ToList();
                return View(q);


            }
            else
            {
                return View(db.Peshats.ToList());
            }







        }

        [HttpGet]
        public ActionResult PESHAdd()
        {
            


            return View();
        }

        [HttpPost]
        public ActionResult PESHAdd(Peshat p1)
        {

            Peshat p = new Peshat();

            p.PESHAID = p1.PESHAID;
            p.NjesiaMatese = p1.NjesiaMatese;
            p.Numri = p1.Numri;

            db.Peshats.Add(p);

            db.SaveChanges();


            return RedirectToAction("PESHAIndex");
        }
        [HttpGet]
        public ActionResult PESHAEdit(int id)
        {
            
            Peshat p = db.Peshats.Where(a => a.PESHAID == id).FirstOrDefault();

            db.SaveChanges();

            return View(p);




        }

        [HttpPost]
        public ActionResult PESHAEdit(Peshat p1)
        {
            Peshat p = db.Peshats.Where(a => a.PESHAID == p1.PESHAID).FirstOrDefault();
            p.PESHAID = p1.PESHAID;
            p.NjesiaMatese = p1.NjesiaMatese;
            p.Numri = p1.Numri;





            db.SaveChanges();

            return RedirectToAction("PESHAIndex");




        }

        public ActionResult PESHADelete(int id)
        {
           
            
            Peshat p = db.Peshats.Where(a => a.PESHAID == id).FirstOrDefault();
            Qmimi q = db.Qmimis.Where(a => a.PeshaId == p.PESHAID).FirstOrDefault();
            if (q != null)
            {

                TempData["Message"] = "Peshat  nuk mund te fshihet sepse ka eshte e lidhur direkt me qmimin!";



            }
            else
            {


                db.Peshats.Remove(p);
                db.SaveChanges();

            }



            return RedirectToAction("PESHAIndex");
        }


        //=======================================================QMIMI=============================================================================
        public ActionResult QMIMIndex(string kerko)
        {
            
            ViewBag.Sherbimi = new SelectList(db.LlojiSherbimits.ToList(), "SHERBIMIID", "EmriSherbimit");
            ViewBag.Pesha = new SelectList(db.Userats.ToList(), "PESHAID", "Numri");

            if (kerko != null)
            {

                var q = db.Qmimis.Where(a => a.SherbimiId.ToString().Contains(kerko) || a.Trafiku.Contains(kerko) || a.PeshaId.ToString().Contains(kerko)|| a.Cmimi.ToString().Contains(kerko)).ToList();
                return View(q);


            }
            else
            {
                return View(db.Qmimis.ToList());
            }







        }

        [HttpGet]
        public ActionResult QMIMIAdd()
        {
            
            ViewBag.Sherbimi = new SelectList(db.LlojiSherbimits.ToList(), "SHERBIMIID", "EmriSherbimit");
            ViewBag.Pesha = new SelectList(db.Peshats.ToList(), "PESHAID", "Numri");

            return View();
        }

        [HttpPost]
        public ActionResult QMIMIAdd(Qmimi q1, int Sherbimi, int Pesha)
        {
            

            Qmimi q = new Qmimi();
            q.SherbimiId = Sherbimi;
            q.PeshaId = Pesha;
            q.Trafiku = q1.Trafiku;
            q.Cmimi = q1.Cmimi;




            db.Qmimis.Add(q);

            db.SaveChanges();


            return RedirectToAction("QMIMIndex");
        }
        [HttpGet]
        public ActionResult QMIMIEdit(int id)
        {
            
            ViewBag.Sherbimi = new SelectList(db.LlojiSherbimits.ToList(), "SHERBIMIID", "EmriSherbimit");
            ViewBag.Pesha = new SelectList(db.Peshats.ToList(), "PESHAID", "Numri");
            Qmimi q = db.Qmimis.Where(a => a.CMIMIID == id).FirstOrDefault();


            db.SaveChanges();

            return View(q);




        }

        [HttpPost]
        public ActionResult QMIMIEdit(Qmimi q1, int Sherbimi, int Pesha)
        {
            
            Qmimi q = db.Qmimis.Where(a => a.CMIMIID == q1.CMIMIID).FirstOrDefault();
            q.SherbimiId = Sherbimi;
            q.PeshaId = Pesha;
            q.Trafiku = q1.Trafiku;
            q.Cmimi = q1.Cmimi;



            db.SaveChanges();

            return RedirectToAction("QMIMIndex");




        }

        public ActionResult QMIMIDelete(int id)
        {
            
            Qmimi q = db.Qmimis.Where(a => a.CMIMIID == id).FirstOrDefault();

            DergesatNderkombetare dn = db.DergesatNderkombetares.Where(a => a.QmimiId == q.CMIMIID).FirstOrDefault();
            DergesatVendore dv = db.DergesatVendores.Where(a => a.QmimiId == q.CMIMIID).FirstOrDefault();

            if (dv != null || dn != null)
            {

                TempData["Message"] = "Qmimi  nuk mund te fshihet sepse ka lidhet me te dhenat rreth dergesave ne te!";



            }
            else
            {


                db.Qmimis.Remove(q);
                db.SaveChanges();

            }



            return RedirectToAction("QMIMIndex");
        }

    }

}

