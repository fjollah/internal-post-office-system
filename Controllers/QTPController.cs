﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POSTA1.Models;
using System.Web.Security;
using Rotativa;
using System.Data.Entity;

namespace POSTA1.Controllers
{
    [Authorize(Roles = "QTP")]
    public class QTPController : Controller
    {
        public static PostaEntities7 db = new PostaEntities7();

        public int GetId()
        {
            var emri = User.Identity.Name;

            var query = from f in db.Userats
                        where f.UserName == emri
                        select f;

            int id2 = Convert.ToInt32(query.FirstOrDefault().UID);
            return id2;
        }

        public int GetSid()
        {
            var emri = User.Identity.Name;

            var query = from f in db.Userats
                        where f.UserName == emri
                        select f;

            int id2 = Convert.ToInt32(query.FirstOrDefault().UID);
            var user = from q in db.Stafis
                       where q.UserID == id2
                       select q;
            int id1 = Convert.ToInt32(user.FirstOrDefault().SID);
            return id1;



        }
        // GET: Admin
        [HttpGet]
        public ActionResult Index()
        {


            int sid = GetSid();

            Stafi st = db.Stafis.Where(x => x.SID == sid).FirstOrDefault();
            return View(st);




        }
        [HttpPost]
        public ActionResult Index(string pass, string passIRI)
        {
            int id = GetId();

            Stafi st = db.Stafis.FirstOrDefault(a => a.UserID == id);


            if (pass == passIRI)
            {
                Userat u = (from x in db.Userats
                            where x.UID == id
                            select x).First();
                u.Fjalekalimi = pass;

                db.SaveChanges();

            }
            else
            {
                TempData["Message"] = "Fjalekalimet duhet te jene te njejt";
            }

            return View(st);
        }
    //============================================================================================================================//
        [HttpGet]
        public ActionResult PostaPranuar(string kerko)
        {
            

            ViewBag.Qytetet = new SelectList(db.Qytetets.ToList(), "QYTETIID", "EmriQytetit");
            ViewBag.Userat = new SelectList(db.Userats.ToList(), "UID", "UserName");
            ViewBag.Sherbimet = new SelectList(db.LlojiSherbimits.ToList(), "SHERBIMIID", "EmriSherbimit");
            ViewBag.Qmimi = new SelectList(db.Qmimis.ToList(), "CMIMIID", "Cmimi");
            ViewBag.Sherbimet = new SelectList(db.Peshats.ToList(), "PESHAID", "Numri");
            ViewBag.Zyrat = new SelectList(db.ZyraPostares.ToList(), "ZPID", "EmriZyres");

        




            if (kerko != null)
            {

                var q = db.DergesatVendores.Where(a => a.NrRekomandes.Contains(kerko) || a.LlojiSherbimit.EmriSherbimit.Contains(kerko) || a.Peshat.Numri.ToString().Contains(kerko)
                     || a.Userat.UserName.Contains(kerko) || a.Qytetet.EmriQytetit.Contains(kerko)
                      || a.Derguesi.Contains(kerko) || a.AdresaDerguesit.Contains(kerko)
                       || a.Marresi.Contains(kerko) || a.AdresaMarresit.Contains(kerko)
                        || a.DataRegj.ToString().Contains(kerko) || a.ZyraPostare.EmriZyres.Contains(kerko)
                        || a.Qmimi.Cmimi.ToString().Contains(kerko)).ToList();

                var q1 = from c in q
                         where !(from o in db.DergesatPerDorezims
                                 select o.DVendoreId)
                                .Contains(c.DVID) 
                         select c;
               
              return View(q1.ToList());


            }

           


            else
            {


              

                var q1 = from c in db.DergesatVendores
                        where !(from o in db.DergesatPerDorezims
                               select o.DVendoreId)
                               .Contains(c.DVID) && c.DataRegj==DateTime.Today
                            select c;


                return View(q1.ToList());
            }


          
           
        }


        public ActionResult LeterDeleteDV(int id)
        {
            DergesatVendore dv=db.DergesatVendores.Where(a=>a.DVID==id).FirstOrDefault();
            //DergesatPerDorezim dd = db.DergesatPerDorezims.Where(a => a.DVendoreId == id).FirstOrDefault();

            
                db.DergesatVendores.Remove(dv);
                db.SaveChanges();


            

            return RedirectToAction("PostaPranuar");


        }
        public ActionResult LeterDeleteDN(int id)
        {
            DergesatNderkombetare dn = db.DergesatNderkombetares.Where(a => a.DNID == id).FirstOrDefault();
            //DergesatPerDorezim dd = db.DergesatPerDorezims.Where(a => a.DVendoreId == id).FirstOrDefault();


            db.DergesatNderkombetares.Remove(dn);
            db.SaveChanges();




            return RedirectToAction("DergesatNderkombetare");


        }





        [HttpGet]
        public ActionResult ShtoNeListe(int id)
        {
            
            ViewBag.ID = id;

         
            
            var query = (from x in db.Userats
                        where x.Grupet.EmriGrupit == "Postier"
                        select x).ToList();
            ViewBag.Postieri = query;

                

                

            return View();
        

        }
        [HttpPost]
        public ActionResult ShtoNeListe(int id,DergesatPerDorezim d1)
        {
            //useri qe eshte aktualisht i loguar (me session ose cookies)
            int id1 = GetId();


          

            DergesatPerDorezim d = new DergesatPerDorezim();
            d.DVendoreId = id;
            d.PostieriId = d1.PostieriId;
            d.Data = DateTime.Today;
            d.UserID = id1;
            d.Pershkrimi = d1.Pershkrimi;
            db.DergesatPerDorezims.Add(d);

            db.SaveChanges();


            return RedirectToAction("PostaDorezim");
        }
    
        public ActionResult PostaDorezim(string kerko)
        {

            ViewBag.Userat = new SelectList(db.Userats.ToList(), "UID", "UserName");

            List<DergesatPerDorezim> d = db.DergesatPerDorezims.Include("Userat").ToList();
            var d1 = from a in d
                     where a.Data == DateTime.Today
                     select a;

            if (kerko == null)
            {
                
                return View(d1.ToList());
            }
            else
            {

                var query1 = from a in d
                             where (a.Userat.UserName.Contains(kerko) || a.Pershkrimi.Contains(kerko) || a.Data.ToString().Contains(kerko))
                             select a;


                return View(query1.ToList());

            }




        }
    public ActionResult ListaDorezuar(string kerko)
        {
            
            List<DergesatDorezuara> d = db.DergesatDorezuaras.Include("Userat").Where(x => x.Statusi == "Dorezuar").ToList();
            ViewBag.Userat = new SelectList(db.Userats.ToList(), "UID", "UserName");
            var d1 = from a in d
                     where a.Data == DateTime.Today
                     select a;

            if (kerko == null)
            {

                return View(d1.ToList());
            }
            else
            {

                var query1 = from a in d
                             where (a.Userat.UserName.Contains(kerko) || a.Pershkrimi.Contains(kerko) || a.Data.ToString().Contains(kerko) || a.Statusi.Contains(kerko))
                             select a;

                return View(query1.ToList());

            }
           
        }

    public ActionResult ListaKthyer(string kerko)
    {
        List<DergesatDorezuara> d = db.DergesatDorezuaras.Include("Userat").Where(x => x.Statusi == "Kthyer").ToList();
        ViewBag.Userat = new SelectList(db.Userats.ToList(), "UID", "UserName");
        var d1 = from a in d
                 where a.Data == DateTime.Today
                 select a;


        if (kerko == null)
        {

            return View(d1.ToList());
        }
        else
        {

            var query1 = from a in d
                         where (a.Userat.UserName.Contains(kerko) || a.Pershkrimi.Contains(kerko) || a.Data.ToString().Contains(kerko) || a.Statusi.Contains(kerko))
                         select a;

            return View(query1.ToList());

        }
           
    }

    public ActionResult DergesatNderkombetare(string kerko)
    {
            
       
        ViewBag.Qytetet = new SelectList(db.Shtetets.ToList(), "SHTETIID", "EmriShtetit");
        ViewBag.Userat = new SelectList(db.Userats.ToList(), "UID", "UserName");
        ViewBag.Sherbimet = new SelectList(db.LlojiSherbimits.ToList(), "SHERBIMIID", "EmriSherbimit");
        ViewBag.Qmimi = new SelectList(db.Qmimis.ToList(), "CMIMIID", "Cmimi");
        ViewBag.Peshat = new SelectList(db.Peshats.ToList(), "PESHAID", "Numri");
        ViewBag.Zyrat = new SelectList(db.ZyraPostares.ToList(), "ZPID", "EmriZyres");
      



        var q1 = from c in db.DergesatNderkombetares
                 where !(from o in db.Transportis
                         select o.DnID)
                        .Contains(c.DNID) && c.DataRegj == DateTime.Today
                 select c;

        if (kerko == null)
        {

            return View(q1.ToList());
        }
        else
        {

            var query1 = db.DergesatNderkombetares.Where(a => a.NrRekomandes.Contains(kerko) || a.LlojiSherbimit.EmriSherbimit.Contains(kerko) || a.Peshat.Numri.ToString().Contains(kerko)
                     || a.Userat.UserName.Contains(kerko) || a.Shtetet.EmriShtetit.Contains(kerko)
                      || a.Derguesi.Contains(kerko) || a.AdresaDerguesit.Contains(kerko)
                       || a.Marresi.Contains(kerko) || a.AdresaMarresit.Contains(kerko)
                        || a.DataRegj.ToString().Contains(kerko) || a.ZyraPostare.EmriZyres.Contains(kerko)
                        || a.Qmimi.Cmimi.ToString().Contains(kerko)).ToList();

            var q2 = from c in query1
                     where !(from o in db.Transportis
                             select o.DnID)
                            .Contains(c.DNID) 
                     select c;



            return View(q2.ToList());

        }
           

  
    }
        [HttpGet]
    public ActionResult Ngarko(int id)
    {

        ViewBag.ID = id;



        var query = (from x in db.Userats
                     where x.Grupet.EmriGrupit == "Postier"
                     select x).ToList();
        ViewBag.Postieri = query;

                

       
        return View();
    }
        [HttpPost]
        public ActionResult Ngarko(int id, Transporti tdn1)
        {
            Transporti tdn = new Transporti();


            tdn.DnID = id;
            tdn.PostieriId = tdn1.PostieriId;
            tdn.Data = DateTime.Today;
            tdn.Rrugetimi = tdn1.Rrugetimi;
            tdn.Pershkrimi = tdn1.Pershkrimi;

            db.Transportis.Add(tdn);

            db.SaveChanges();


            return RedirectToAction("TransportiNderkombetar");
           

        }

    public ActionResult TransportiNderkombetar(string kerko)
    {

        List<Transporti> d = db.Transportis.ToList();
        ViewBag.Userat = new SelectList(db.Userats.ToList(), "UID", "UserName");
     

        if (kerko == null)
        {
            var qu = (from c in d
                      where (c.Data == DateTime.Today)
                      select c).ToList();


            return View(qu);
        }
        else
        {

            var query1 = db.Transportis.Where(a => a.Userat.UserName.Contains(kerko) || a.Pershkrimi.Contains(kerko) || a.Data.ToString().Contains(kerko) || a.Rrugetimi.Contains(kerko)).ToList();
                       

            return View(query1);

        }

       
     
    }

    public ActionResult Kerkesat(string kerko)
    {
        List<Kerkesat> k = db.Kerkesats.ToList();
        var d1 = from a in k
                 where (a.Data == DateTime.Today)
                 select a;
        if (kerko == null)
        {

            return View(d1.ToList());
        }
        else
        {

            var query1 = from a in k
                         where (a.Userat.UserName.Contains(kerko) || a.Pershkrimi.Contains(kerko) || a.Data.ToString().Contains(kerko))
                         select a;

            return View(query1.ToList());

        }
         

    }


    [HttpGet]
    public ActionResult Pergjigju(int id)
    {

        ViewBag.ID = id;
       
      

ViewBag.Useri=new SelectList(db.Userats.ToList(),"UID","UserName");




        return View();
    }
    [HttpPost]
    public ActionResult Pergjigju(int id, PergjigjjaKerkes pk1)
    {
        //useri qe eshte aktualisht i loguar (me session ose cookies)
        int id1 = GetId();

        PergjigjjaKerkes k = new PergjigjjaKerkes();
         var u = (from x in db.Kerkesats
                    where x.KERKESAID== id
                    select x.UserId).First();
        k.KerkesaId = id;
        k.UserId = id1;
        k.MarresiPergjigjjes = u;
        k.Data = DateTime.Today;
        k.Pershkrimi = pk1.Pershkrimi;

        db.PergjigjjaKerkeses.Add(k);

        db.SaveChanges();


        return RedirectToAction("Pergjigjjet");


    }






    public ActionResult Pergjigjjet(string kerko)
    {
            

        List<PergjigjjaKerkes> pk = db.PergjigjjaKerkeses.ToList();
        ViewBag.Useri = new SelectList(db.Userats.ToList(), "UID", "UserName");
        var d1 = from a in pk
                 where a.Data == DateTime.Today
                 select a;


        if (kerko == null)
        {

            return View(d1.ToList());
        }
        else
        {

            var query1 = from a in pk
                         where (a.Userat.UserName.Contains(kerko) || a.Pershkrimi.Contains(kerko) || a.Data.ToString().Contains(kerko))
                         select a;

            return View(query1.ToList());

        }
         
    }
  
   

}}