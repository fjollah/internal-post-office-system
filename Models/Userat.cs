//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace POSTA1.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Userat
    {
        public Userat()
        {
            this.DergesatDorezuaras = new HashSet<DergesatDorezuara>();
            this.DergesatNderkombetares = new HashSet<DergesatNderkombetare>();
            this.DergesatNderkombetares1 = new HashSet<DergesatNderkombetare>();
            this.DergesatPerDorezims = new HashSet<DergesatPerDorezim>();
            this.DergesatPerDorezims1 = new HashSet<DergesatPerDorezim>();
            this.DergesatPerDorezims2 = new HashSet<DergesatPerDorezim>();
            this.DergesatPerDorezims3 = new HashSet<DergesatPerDorezim>();
            this.DergesatVendores = new HashSet<DergesatVendore>();
            this.Kerkesats = new HashSet<Kerkesat>();
            this.PergjigjjaKerkeses = new HashSet<PergjigjjaKerkes>();
            this.PergjigjjaKerkeses1 = new HashSet<PergjigjjaKerkes>();
            this.Stafis = new HashSet<Stafi>();
            this.Transportis = new HashSet<Transporti>();
        }
    
        public int UID { get; set; }
        public string UserName { get; set; }
        public string Fjalekalimi { get; set; }
        public int GrupiID { get; set; }
    
        public virtual ICollection<DergesatDorezuara> DergesatDorezuaras { get; set; }
        public virtual ICollection<DergesatNderkombetare> DergesatNderkombetares { get; set; }
        public virtual ICollection<DergesatNderkombetare> DergesatNderkombetares1 { get; set; }
        public virtual ICollection<DergesatPerDorezim> DergesatPerDorezims { get; set; }
        public virtual ICollection<DergesatPerDorezim> DergesatPerDorezims1 { get; set; }
        public virtual ICollection<DergesatPerDorezim> DergesatPerDorezims2 { get; set; }
        public virtual ICollection<DergesatPerDorezim> DergesatPerDorezims3 { get; set; }
        public virtual ICollection<DergesatVendore> DergesatVendores { get; set; }
        public virtual Grupet Grupet { get; set; }
        public virtual ICollection<Kerkesat> Kerkesats { get; set; }
        public virtual ICollection<PergjigjjaKerkes> PergjigjjaKerkeses { get; set; }
        public virtual ICollection<PergjigjjaKerkes> PergjigjjaKerkeses1 { get; set; }
        public virtual ICollection<Stafi> Stafis { get; set; }
        public virtual ICollection<Transporti> Transportis { get; set; }
    }
}
