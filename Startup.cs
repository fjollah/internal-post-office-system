﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(POSTA1.Startup))]
namespace POSTA1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
